

import cv2
import numpy as np

kamera=cv2.VideoCapture(0)
dusuk=np.array([0,134,0])
yuksek=np.array([179,229,255])
dusuk1=np.array([0,89,0])
yuksek1=np.array([179,255,255])
dusuk2=np.array([26,72,124])
yuksek2=np.array([81,230,215])
while True:

    ret,goruntu=kamera.read()
    hsv=cv2.cvtColor(goruntu,cv2.COLOR_BGR2HSV)
    mask=cv2.inRange(hsv,dusuk,yuksek)
    mask1=cv2.inRange(hsv,dusuk1,yuksek1)
    mask2=cv2.inRange(hsv,dusuk2,yuksek2)
    son_resim=cv2.bitwise_and(goruntu,goruntu,mask=mask)
    son_resim1=cv2.bitwise_and(goruntu,goruntu,mask=mask1)
    son_resim2=cv2.bitwise_and(goruntu,goruntu,mask=mask2)
    re=cv2.add(son_resim,son_resim1,son_resim2)

    cv2.imshow('Mask',mask)
    cv2.imshow('HSV',hsv)
    cv2.imshow('Son resim',re)
    if cv2.waitKey(25) & 0xFF == ord('q'):
        break
kamera.release()
cv2.destroyAllWindows()